
import { Event } from 'event';
import { Eventpubs } from 'eventpubs';
import { Footer } from 'footer';
import { Header } from 'header';
import { Headerpubs } from 'headerpubs';
import React from 'react';
import ReactDom from 'react-dom';

ReactDom.render(
    <>
    <Header />
    < Headerpubs />
    < Event />
    < Eventpubs />
    < Footer />
    </>,
    document.getElementById('coreContainer')
);
