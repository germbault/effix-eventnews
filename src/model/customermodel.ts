export class CustomerModel {
    public customerId: number;
    public userId_wordpress: number;
    public username: string;
    public customer_site?: string;
    public text?: string;
    public image?: string;
    public publicityActive: string;

    public static fromJSON(jsonCustomerModel: CustomerModel) {
        const customerModel = new CustomerModel;
        Object.assign(customerModel, jsonCustomerModel);
        return customerModel;
    }
}
