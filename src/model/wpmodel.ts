export class WpModel {
    public id: number;
    public categories: number;
    public title: string;
    public content: string;
    public source_url: string;
    // public img_title: string;
    // public categoriesId: number;
    public featured_media: number;

    public static fromJSON(jsonWpModel: WpModel) {
        const wpModel = new WpModel;
        Object.assign(wpModel, jsonWpModel);
        return wpModel;
    }
}
