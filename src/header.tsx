import React from 'react';

export class Header extends React.Component<{}>{

    public render() {
        return <>
            <section>
                <div className='container'>
                    <div className='container-flex-nav'>
                        <a href='https://germain.devwebgarneau.com/effix-ecommerce/' target='blank'>
                            <img src='/img/main.png' alt='main' />
                        </a>
                        <a href='http://effix-landtwo.germain.devwebgarneau.com/' target='blank'><img className='logo' src='/img/logo-effix-ads-black.svg' alt='logo Effix' /></a>
                    </div>
                    <h1 className='h1-title'>Évenements de Effix & ads</h1>
                    <img src='img/groupe.png ' alt='Groupe festif' />
                </div>
            </section>
        </>;
    }
}
