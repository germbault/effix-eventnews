import { Pub } from 'pub';
import React from 'react';
import { CustomerModel } from './model/customermodel';

interface Props { }
interface State {
  cmsPub: CustomerModel;
}

export class Headerpubs extends React.Component<Props, State> {


  constructor(props: Props) {
    super(props);

    this.state = {
      cmsPub: new CustomerModel,
    };
  }

  public render() {
    return <>
      <section>
        <div className='publicite'>
          <div className='pub' >
            {<Pub />}
          </div>
          <div className='pub' >
            {<Pub />}
          </div>
          <div className='pub' >
            {<Pub />}
          </div>
        </div>
      </section>
    </>;
  }
}
