import { ApiWp } from 'apiwp';
import { WpModel } from 'model/wpmodel';
import React from 'react';


interface Props { }
interface State {
    articles: WpModel[];
}

export class Event extends React.Component<Props, State> {
    private apiWp = new ApiWp;

    constructor(props: Props) {
        super(props);

        this.state = {
            articles: [],
        };
    }

    public async componentDidMount() {

        const wpPosts = (await this.apiWp.getJson('/wp-json/wp/v2/posts?per_page=100') as any[]).map(WpModel.fromJSON);
        wpPosts.map(async post => {
            const postCatetories = post.categories.toString();

            if (post.id > 0 && postCatetories === '19') {
                const id = post.id;
                const title = post.title.rendered;
                let content = post.content.rendered;

                const categories = parseInt(post.categories.toString());
                const featured_media = post.featured_media;

                content = content.replace(/(<([^>]+)>)/gi, '');

                // console.log('id : ' + typeof (id));
                // console.log('title : ' + title);
                // console.log('title : ' + typeof (title));
                console.log('content : ' + content);
                // console.log('content : ' + typeof (content));
                // console.log('categories : ' + typeof (categories));
                // console.log('featured_media : ' + typeof (featured_media));
                // console.log(post);
                // console.log(parseInt(post.categories.toString()));
                // console.log('Post categorie: ' + post.categories.toString());
                // console.log('Post featured Media: ' + post.featured_media);
                // console.log(post.featured_media);

                const wpPostsMedia = (await this.apiWp.getJson('/wp-json/wp/v2/media/' + post.featured_media) as WpModel);
                const source_url = wpPostsMedia.source_url;

                // console.log('source_url : ' + typeof (source_url));
                // console.log(wpPostsMedia);
                // console.log('/media/' + post.featured_media);

                const articlesInfo = {
                    id: id,
                    title: title,
                    content: content,
                    categories: categories,
                    featured_media: featured_media,
                    source_url: source_url,
                };

                this.state.articles.push(articlesInfo);
                this.setState({ articles: this.state.articles });

            } else {
                console.log('Aucun article a été trouvé ou pas la bonne catégorie!');
            }
        });
    }

    public render() {
        const { articles } = this.state;
        return <>
            <section>
                <div className='container'>
                    {!articles ? 'Chargement...' : articles.map(article =>
                        <div className='row line' key={article.id}>
                            <div className='col-lg-5'>
                                <div className='row'>
                                    <h2 className='h2-title'>
                                        {article.title}
                                    </h2>
                                    <p className='texte'>{article.content}</p>
                                    {/* <p className='texte'>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orci.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orcLorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orci.Lorem ipsum dolor sit amet.
                                    </p> */}
                                </div>
                            </div>
                            <div className='col-lg-7'>
                                <img src={article.source_url} alt='BBQ' />
                            </div>
                        </div>
                        // <div className='line' />
                    )}
                </div>
            </section>
        </>;
    }



    // public render() {
    //     return <>
    //         <section>
    //             <div className='container'>
    //                 <div className='row'>
    //                     <div className='col-lg-5'>
    //                         <div className='row'>
    //                             <h2 className='h2-title'>
    //                                 Lorem ipsum dolor
    //                             </h2>
    //                             <p className='texte'>
    //                                 Lorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orci.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orcLorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orci.Lorem ipsum dolor sit amet.
    //                             </p>
    //                         </div>
    //                     </div>
    //                     <div className='col-lg-7'>
    //                         <img src='../../img/bbq.png' alt='BBQ' />
    //                     </div>
    //                 </div>
    //                 <div className='line' />
    //                 <div className='row'>
    //                     <div className='col-lg-5'>
    //                         <div className='row'>
    //                             <h2 className='h2-title'>
    //                                 Lorem ipsum dolor
    //                             </h2>
    //                             <p className='texte'>
    //                                 Lorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orci.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orcLorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orci.Lorem ipsum dolor sit amet.
    //                             </p>
    //                         </div>
    //                     </div>
    //                     <div className='col-lg-7'>
    //                         <img src='../../img/party.png' alt='Party' />
    //                     </div>
    //                 </div>
    //                 <div className='line' />
    //                 <div className='row'>
    //                     <div className='col-lg-5'>
    //                         <div className='row'>
    //                             <h2 className='h2-title'>
    //                                 Lorem ipsum dolor
    //                             </h2>
    //                             <p className='texte'>
    //                                 Lorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orci.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orcLorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orci.Lorem ipsum dolor sit amet.
    //                             </p>
    //                         </div>
    //                     </div>
    //                     <div className='col-lg-7'>
    //                         <img src='../../img/tanks.png' alt='Thanksgiving' />
    //                     </div>
    //                 </div>
    //                 <div className='line' />
    //                 <div className='row'>
    //                     <div className='col-lg-5'>
    //                         <div className='row'>
    //                             <h2 className='h2-title'>
    //                                 Lorem ipsum dolor
    //                             </h2>
    //                             <p className='texte'>
    //                                 Lorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orci.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orcLorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orci.Lorem ipsum dolor sit amet.
    //                             </p>
    //                         </div>
    //                     </div>
    //                     <div className='col-lg-7'>
    //                         <img src='../../img/halloween.png' alt='Halloween' />
    //                     </div>
    //                 </div>
    //                 <div className='line' />
    //                 <div className='row'>
    //                     <div className='col-lg-5'>
    //                         <div className='row'>
    //                             <h2 className='h2-title'>
    //                                 Lorem ipsum dolor
    //                             </h2>
    //                             <p className='texte'>
    //                                 Lorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orci.Lorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orcLorem ipsum dolor sit amet, consectetur adipiscing elit.Nulla egestas malesuada dolor aliquam sapien ut morbi gravida orci.Lorem ipsum dolor sit amet.
    //                             </p>
    //                         </div>
    //                     </div>
    //                     <div className='col-lg-7'>
    //                         <img src='../../img/noel.png' alt='Noël' />
    //                     </div>
    //                 </div>
    //                 <div className='line' />
    //             </div>
    //         </section>
    //     </>;
    // }
}
