import React from 'react';

export class Footer extends React.Component<{}>{

    public render() {
        return <>
            <footer>
                <div className='container'>
                    <div className='flex-button'>
                        <a href='https://germain.devwebgarneau.com/effix-ecommerce/' target='blank'>
                            <button className='button'>S'inscrire à l'info lettre</button>
                        </a>
                    </div>
                </div>
            </footer>
        </>;
    }
}
